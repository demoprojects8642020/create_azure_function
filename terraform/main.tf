provider "azurerm" {
  skip_provider_registration = true
  features {}
}

resource "azurerm_app_service_plan" "demo" {
  name                = "demo-appserviceplan${random_integer.function_suffix.result}"
  location            = "Southeast Asia"
  resource_group_name = "aksdemo"
  kind                = "FunctionApp"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "random_integer" "function_suffix" {
  min = 1000
  max = 9999
}

resource "azurerm_storage_account" "demo" {
  name                     = "demostorageaccount${random_integer.function_suffix.result}"
  resource_group_name      = azurerm_app_service_plan.demo.resource_group_name
  location                 = azurerm_app_service_plan.demo.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "staging"
  }
}

resource "azurerm_function_app" "demo" {
  name                      = "demo-function-app-${random_integer.function_suffix.result}"
  location                  = azurerm_app_service_plan.demo.location
  resource_group_name       = azurerm_app_service_plan.demo.resource_group_name
  app_service_plan_id       = azurerm_app_service_plan.demo.id 
  storage_account_name      = azurerm_storage_account.demo.name
  storage_account_access_key = azurerm_storage_account.demo.primary_access_key
  version                   = "~4"
}
